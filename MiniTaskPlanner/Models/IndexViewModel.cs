﻿using System.Collections.Generic;
using System.Linq;

namespace MiniTaskPlanner.Models
{
    public class IndexViewModel
    {
        List<Employee> employees = new List<Employee>();
        List<Task> tasks = new List<Task>();

        public List<Employee> Employees { get { return employees; } }
        public List<Task> Tasks { get { return tasks; } }

        public IndexViewModel()
        {
            using (MiniTaskPlannerEntities context = new MiniTaskPlannerEntities())
            {
                employees = context.Employees.ToList();
                tasks = context.Tasks.ToList();
            }
        }
    }
}